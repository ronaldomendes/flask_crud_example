# Flask CRUD Example 

Projeto desenvolvido a partir da playlist disponível no [YouTube](https://www.youtube.com/playlist?list=PL3BqW_m3m6a05ALSBW02qDXmfDKIip2KX).

Para atualizar as dependências do projeto:

`pip freeze > requirements.txt`

Para instalar as dependências do projeto:

`pip install -r requirements.txt`

Para iniciar a aplicação:

`python run.py runserver`

Para criar o banco de dados:

`python run.py db init`

`python run.py db migrate`

`python run.py db upgrade`