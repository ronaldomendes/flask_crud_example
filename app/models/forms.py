from flask_wtf import FlaskForm
from wtforms import PasswordField, BooleanField, StringField
from wtforms.validators import Length, DataRequired, Regexp


class LoginForm(FlaskForm):
    username = StringField("username",
                           validators=[
                               Length(min=6, max=100, message="Username must have at least 6 characters"),
                               DataRequired("Username is a required field")])
    password = PasswordField("password",
                             validators=[
                                 Length(min=8, max=100, message="Password must have between 8 and 100 characters"),
                                 DataRequired("Password is a required field"),
                                 Regexp("[^ \t\r\n\f]", 0, message="Password must not contain whitespaces")])
    remember_me = BooleanField("remember_me")
