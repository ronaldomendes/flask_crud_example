from flask import render_template, flash, url_for, redirect
from flask_login import login_user, logout_user

from app import app, db, login_manager
from app.models.forms import LoginForm
from app.models.tables import User


@login_manager.user_loader
def load_user(id):
    return User.query.filter_by(id=id).first()


@app.route("/index")
@app.route("/")
def index():
    return render_template('index.html')


@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        u = User.query.filter_by(username=form.username.data).first()
        if u and u.password == form.password.data:
            login_user(u)
            flash("Logged in")
            return redirect(url_for("index"))
        else:
            flash("Invalid user or credentials")
    return render_template('login.html', form=form)


@app.route("/logout")
def logout():
    logout_user()
    flash("Logged out")
    return redirect(url_for("index"))


@app.route("/teste/<info>")
@app.route("/teste", defaults={"info": None})
def teste(info):
    # CREATE
    # u = User("godo.santos", "senha123", "Godofredo Santos", "godo.santos@email.com")
    # db.session.add(u)
    # db.session.commit()

    # READ
    # um registro
    # u = User.query.filter_by(username="godo.santos").first()
    # print(u)
    # todos os registros
    # u = User.query.filter_by(username="godo.santos").all()
    # print(u.name, u.password)

    # UPDATE
    # u = User.query.filter_by(username="godo.santos").first()
    # u.name = "Godofredo Santos Souza"
    # db.session.add(u)
    # db.session.commit()

    # DELETE
    # u = User.query.filter_by(username="godo.santos").first()
    # db.session.delete(u)
    # db.session.commit()
    return "OK"
